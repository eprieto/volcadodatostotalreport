﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crearDatos
{
    public class FormatoToBe
    {
        [BsonElement("Consolidado")]
        public string Consolidado { get; set; }
        [BsonElement("Entidad")]
        public string Entidad { get; set; }
        [BsonElement("Fecha")]
        public string Fecha { get; set; }
        [BsonElement("Usuario")]
        public string Usuario { get; set; }
        [BsonElement("Registro_descripcion")]
        public string Registro_Descripcion { get; set; }
        [BsonElement("Registro_ordenador")]
        public string Registro_Ordenador { get; set; }
        [BsonElement("Registro_tipo")]
        public string Registro_Tipo { get; set; }
        [BsonElement("Campos")]
        public List<Campos> Campos { get; set; }

    }

    public class Campos
    {
        [BsonElement("Orden")]
        public string Orden { get; set; }
        [BsonElement("Campo_IdLog")]
        public string campo_IdLog { get; set; }
        [BsonElement("Campo_Nombre")]
        public string campo_nombre { get; set; }
        [BsonElement("Campo_Valor")]
        public string campo_valor { get; set; }
        [BsonElement("Campo_Posicion")]
        public string campo_posicion { get; set; }
        [BsonElement("Campo_Funcion")]
        public string campo_Funcion { get; set; }
        [BsonElement("Campo_Horiz")]
        public string campo_Horiz { get; set; }
        [BsonElement("Campo_Vert")]
        public string campo_Vert { get; set; }

    }

    public class ConsolidadoEncabezado
    {
        [BsonElement("Consolidado")]
        public string Consolidado { get; set; }
        [BsonElement("Id_Formato")]
        public string Id_Formato { get; set; }
        [BsonElement("Formato")]
        public string Formato { get; set; }
        [BsonElement("Codigo_Entidad")]
        public string Codigo_Entidad { get; set; }
        [BsonElement("Entidad")]
        public string Entidad { get; set; }
        [BsonElement("Tipo_Entidad")]
        public string Tipo_Entidad { get; set; }
        [BsonElement("Sector")]
        public string Sector { get; set; }
        public string Fecha_Creacion { get; set; }
        [BsonElement("Fecha_Modificacion")]
        public string Fecha_Modificacion { get; set; }
        [BsonElement("Usuario")]
        public string Usuario { get; set; }
        [BsonElement("Estado_Volcado")]
        public string Estado_Volcado { get; set; }
        [BsonElement("Error")]
        public string Error { get; set; }

    }
}
