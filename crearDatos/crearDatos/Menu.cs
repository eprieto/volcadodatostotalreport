﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace crearDatos
{
    class Menu
    {
        static void Main(string[] args)
        {
            // Generar archivo plano
            //string path = @"C:\\Temp\Datos_Vol_Fogafin.txt";
            //FileStream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
            var json_serializer = new JavaScriptSerializer();
            Random rnd = new Random();
            int count = 1, contador = 1;
            int max = 100;
            string jsonCabecera = "", jsonDetalle = "";

            try
            {
                ArrayList listaNombres = new ArrayList();
                listaNombres.Add("DIEGO"); listaNombres.Add("FERNANDO");
                listaNombres.Add("CARLOS"); listaNombres.Add("ANDRES");
                listaNombres.Add("LUIS"); listaNombres.Add("EDWIN");
                listaNombres.Add("FRANK"); listaNombres.Add("NICOLAS");
                listaNombres.Add("ALEJANDRO"); listaNombres.Add("TOMAS");
                listaNombres.Add("JUAN"); listaNombres.Add("CAMILO");
                listaNombres.Add("PEDRO"); listaNombres.Add("PAUBLO");
                listaNombres.Add("MATEO"); listaNombres.Add("ELIZABET");
                listaNombres.Add("ANDREA"); listaNombres.Add("CAROLINA");
                listaNombres.Add("INGRID"); listaNombres.Add("LIZETH");
                listaNombres.Add("LAURA"); listaNombres.Add("SILVIA");
                listaNombres.Add("ALBERTO"); listaNombres.Add("JAVIER");
                listaNombres.Add("VICTOR"); listaNombres.Add("SEBASTIAN");
                listaNombres.Add("DIANA"); listaNombres.Add("SANDRA");
                listaNombres.Add("JHONATAN"); listaNombres.Add("PAOLA");
                listaNombres.Add("JENNIFER"); listaNombres.Add("DANIEL");
                listaNombres.Add("EDUARDO"); listaNombres.Add("EDGAR");
                listaNombres.Add("SOFIA"); listaNombres.Add("RAUL");
                listaNombres.Add("MARIA"); listaNombres.Add("JULIAN");
                listaNombres.Add("JHOAN"); listaNombres.Add("JOSE");
                listaNombres.Add("NATHALIA"); listaNombres.Add("VIVIANA");
                listaNombres.Add("GABRIEL"); listaNombres.Add("JARRY");
                listaNombres.Add("FABIAN"); listaNombres.Add("MAURICIO");
                listaNombres.Add("TATIANA"); listaNombres.Add("MARTA");
                listaNombres.Add("ERIKA"); listaNombres.Add("JULY");
                listaNombres.Add("PATRICIA"); listaNombres.Add("CAMILA");
                listaNombres.Add("LILIANA"); listaNombres.Add("CLAUDIA");
                listaNombres.Add("JORGE"); listaNombres.Add("JEFERSON");
                listaNombres.Add("ROMAN"); listaNombres.Add("JEISON");
                listaNombres.Add(""); listaNombres.Add("");

                ArrayList listaApellidos = new ArrayList();            
                listaApellidos.Add("SOLER"); listaApellidos.Add("FLOREZ");
                listaApellidos.Add("RODRIGUEZ"); listaApellidos.Add("GOMEZ");
                listaApellidos.Add("LOPEZ"); listaApellidos.Add("CONZALEZ");
                listaApellidos.Add("GARCIA"); listaApellidos.Add("MARTINEZ");
                listaApellidos.Add("RAMIREZ"); listaApellidos.Add("SANCHEZ");
                listaApellidos.Add("PREREZ"); listaApellidos.Add("MEJIA");
                listaApellidos.Add("HERNANDEZ"); listaApellidos.Add("DIAZ");
                listaApellidos.Add("TORRES"); listaApellidos.Add("ROJAS");
                listaApellidos.Add("VARGAZ"); listaApellidos.Add("MORENO");
                listaApellidos.Add("GUTIERREZ"); listaApellidos.Add("CASTRO");
                listaApellidos.Add("MUÑOZ"); listaApellidos.Add("JIMENEZ");
                listaApellidos.Add("ORTIZ"); listaApellidos.Add("ALVAREZ");
                listaApellidos.Add("RUIZ"); listaApellidos.Add("SUAREZ");
                listaApellidos.Add("ROMERO"); listaApellidos.Add("SALAZAR");
                listaApellidos.Add("HERRERA"); listaApellidos.Add("VALENCIA");
                listaApellidos.Add("QUINTERO"); listaApellidos.Add("RESTREPO");
                listaApellidos.Add("MOLALES"); listaApellidos.Add("MEJIA");
                listaApellidos.Add("ARIAS"); listaApellidos.Add("PARRA");
                listaApellidos.Add("CARDENAS"); listaApellidos.Add("OSOSRIO");
                listaApellidos.Add("CASTILLO"); listaApellidos.Add("CARDONA");
                listaApellidos.Add("MEDINA"); listaApellidos.Add("RIVERA");
                listaApellidos.Add("CORTES"); listaApellidos.Add("CORREA");
                listaApellidos.Add("MARIN"); listaApellidos.Add("RINCON");
                listaApellidos.Add("ZAPATA"); listaApellidos.Add("ESCOBAR");
                listaApellidos.Add("URIBE"); listaApellidos.Add("SAMPER");
                listaApellidos.Add("DUARTE"); listaApellidos.Add("MARTINEZ");
                listaApellidos.Add("FUENTES"); listaApellidos.Add("ROJAS");
                listaApellidos.Add("MOLINA"); listaApellidos.Add("LUNA");
                listaApellidos.Add(""); listaApellidos.Add("");

                ArrayList listaMail = new ArrayList();
                listaMail.Add("eprieto@asesoftware.com"); listaMail.Add("ctrujillo@asesoftware.com");
                listaMail.Add("epena@asesoftware.com"); listaMail.Add("ddiaz@asesoftware.com");
                listaMail.Add("srojas@asesoftware.com"); listaMail.Add("efuentes@asesoftware.com");
                listaMail.Add("smora@asesoftware.com"); listaMail.Add("msarmiento@asesoftware.com");
                listaMail.Add("jdiaz@asesoftware.com"); listaMail.Add("slopez@asesoftware.com");
                listaMail.Add("NA"); listaMail.Add("");

                ArrayList listaNit = new ArrayList();
                listaNit.Add("8300380851"); listaNit.Add("8600370136");
                listaNit.Add("224357238"); listaNit.Add("31169150");
                listaNit.Add("16284431"); listaNit.Add("8600073361");
                listaNit.Add("8001538351"); listaNit.Add("8300540762");
                listaNit.Add("8300537006"); listaNit.Add("8300841063");
                listaNit.Add("NA"); listaNit.Add("");

                ArrayList listaDV = new ArrayList();
                listaDV.Add("1"); listaDV.Add("6");
                listaDV.Add("2"); listaDV.Add("7");
                listaDV.Add("3"); listaDV.Add("8");
                listaDV.Add("4"); listaDV.Add("9");
                listaDV.Add("5"); listaDV.Add("0");
                listaDV.Add("N"); listaDV.Add("");

                ArrayList listaRazonSocial = new ArrayList();
                listaRazonSocial.Add("LA EQUIDAD SEGUROS DE VIDA O.C."); listaRazonSocial.Add("THE WARRANTY GROUP COLOMBIA S.A.");
                listaRazonSocial.Add("MILTONIA SAS"); listaRazonSocial.Add("OLD MUTUAL FONDO DE PENSIONES VOLUNTARIAS");
                listaRazonSocial.Add("CAJA COLOMBIANA DE SUBSIDIO FAMILIAR COLSUBSIDIO"); listaRazonSocial.Add("FONDO DE INVERSION COLECTIVA ABIERTO RENTAR");
                listaRazonSocial.Add("MILTONIA SAS"); listaRazonSocial.Add("FONDO MUTUO DE INVERSION AVANZAR");
                listaRazonSocial.Add("NA"); listaRazonSocial.Add("");

                ArrayList listaTelefono = new ArrayList();
                listaTelefono.Add("8380851"); listaTelefono.Add("8600370");
                listaTelefono.Add("2243572"); listaTelefono.Add("3116910");
                listaTelefono.Add("1628441"); listaTelefono.Add("8600073");
                listaTelefono.Add("8001538"); listaTelefono.Add("8300540");
                listaTelefono.Add("8300537"); listaTelefono.Add("8300841");
                listaTelefono.Add("NA"); listaTelefono.Add("");

                ArrayList listaTipoCliente = new ArrayList();
                listaTipoCliente.Add("1"); listaTipoCliente.Add("0");
                listaTipoCliente.Add("NA"); listaTipoCliente.Add("");

                ArrayList listaDireccion = new ArrayList();
                listaDireccion.Add("CR 64B 49 30"); listaDireccion.Add("KR 64B 49A 30");
                listaDireccion.Add("KR 64 B 49 A 30"); listaDireccion.Add("CL 26 25 50 PI 9");
                listaDireccion.Add("CL 13 A 29 24"); listaDireccion.Add("CR 13 26A 47 P 10");
                listaDireccion.Add("AV EL DORADO 68 B 31 P 7"); listaDireccion.Add("AV CL 100 19 54 OF 302");
                listaDireccion.Add("NA"); listaDireccion.Add("");

                ArrayList listaTipoCliente2 = new ArrayList();
                listaTipoCliente2.Add("1"); listaTipoCliente2.Add("6");
                listaTipoCliente2.Add("2"); listaTipoCliente2.Add("7");
                listaTipoCliente2.Add("3"); listaTipoCliente2.Add("8");
                listaTipoCliente2.Add("4"); listaTipoCliente2.Add("9");
                listaTipoCliente2.Add("5"); listaTipoCliente2.Add("0");
                listaTipoCliente2.Add("N"); listaTipoCliente2.Add("");

                ArrayList listaTipoIdent = new ArrayList();
                listaTipoIdent.Add("1"); listaTipoIdent.Add("2");
                listaTipoIdent.Add("3"); listaTipoIdent.Add("");

                ArrayList listaCodDepa = new ArrayList();
                listaCodDepa.Add("01"); listaCodDepa.Add("06");
                listaCodDepa.Add("02"); listaCodDepa.Add("07");
                listaCodDepa.Add("03"); listaCodDepa.Add("08");
                listaCodDepa.Add("04"); listaCodDepa.Add("09");
                listaCodDepa.Add("05"); listaCodDepa.Add("11");
                listaCodDepa.Add("N"); listaCodDepa.Add("");

                ArrayList listaCodeMuni = new ArrayList();
                listaCodeMuni.Add("001"); listaCodeMuni.Add("006");
                listaCodeMuni.Add("002"); listaCodeMuni.Add("007");
                listaCodeMuni.Add("003"); listaCodeMuni.Add("008");
                listaCodeMuni.Add("004"); listaCodeMuni.Add("009");
                listaCodeMuni.Add("005"); listaCodeMuni.Add("011");
                listaCodeMuni.Add("N"); listaCodeMuni.Add("");

                ArrayList listaCelular = new ArrayList();
                listaCelular.Add("3121245441"); listaCelular.Add("3121245445");
                listaCelular.Add("3121245442"); listaCelular.Add("3121245446");
                listaCelular.Add("3121245443"); listaCelular.Add("3121245447");
                listaCelular.Add("3121245444"); listaCelular.Add("3121245448");
                listaCelular.Add("N"); listaCelular.Add("");

                ArrayList listaTipoDeposito = new ArrayList();
                listaTipoDeposito.Add("01"); listaTipoDeposito.Add("06");
                listaTipoDeposito.Add("02"); listaTipoDeposito.Add("07");
                listaTipoDeposito.Add("03"); listaTipoDeposito.Add("08");
                listaTipoDeposito.Add("04"); listaTipoDeposito.Add("09");
                listaTipoDeposito.Add("05"); listaTipoDeposito.Add("11");
                listaTipoDeposito.Add("N"); listaTipoDeposito.Add("");

                ArrayList listaClaseAhorro = new ArrayList();
                listaClaseAhorro.Add("1"); listaClaseAhorro.Add("2");
                listaClaseAhorro.Add("N"); listaClaseAhorro.Add("");

                ArrayList listaNumeroProd = new ArrayList();
                listaNumeroProd.Add("124141514"); listaNumeroProd.Add("124141515");
                listaNumeroProd.Add("124141521"); listaNumeroProd.Add("");

                ArrayList listaEstadoCap = new ArrayList();
                listaEstadoCap.Add("1"); listaEstadoCap.Add("2");
                listaEstadoCap.Add("N"); listaEstadoCap.Add("");

                ArrayList listaTipoCuenta = new ArrayList();
                listaTipoCuenta.Add("1"); listaTipoCuenta.Add("2");
                listaTipoCuenta.Add("N"); listaTipoCuenta.Add("");

                ArrayList listaIdTitular = new ArrayList();
                listaIdTitular.Add("1"); listaIdTitular.Add("2");
                listaIdTitular.Add("N"); listaIdTitular.Add("");

                ArrayList listaFechaSaldoCap = new ArrayList();
                listaFechaSaldoCap.Add("4/30/2020 12:00:00 AM"); listaFechaSaldoCap.Add("2/31/2020 12:00:00 AM");
                listaFechaSaldoCap.Add("N"); listaFechaSaldoCap.Add("");

                ArrayList listaSaldoCap = new ArrayList();
                listaSaldoCap.Add("16.497.915"); listaSaldoCap.Add("8.106.518");
                listaSaldoCap.Add("11.300.511"); listaSaldoCap.Add("14.822.547");
                listaSaldoCap.Add("9.882.909"); listaSaldoCap.Add("21.612.373");
                listaSaldoCap.Add("N"); listaSaldoCap.Add("");

                ArrayList listaValorInteres = new ArrayList();
                listaValorInteres.Add("494.937,45"); listaValorInteres.Add("243.195,54");
                listaValorInteres.Add("339.015,33"); listaValorInteres.Add("444.676,41");
                listaValorInteres.Add("296.487,27"); listaValorInteres.Add("314.337,93");
                listaValorInteres.Add("N"); listaValorInteres.Add("");

                ArrayList listaCodMoneda = new ArrayList();
                listaCodMoneda.Add("1"); listaCodMoneda.Add("2");
                listaCodMoneda.Add("N"); listaCodMoneda.Add("");

                ArrayList listaEstatusPropiedad = new ArrayList();
                listaEstatusPropiedad.Add("1"); listaEstatusPropiedad.Add("2");
                listaEstatusPropiedad.Add("3"); listaEstatusPropiedad.Add("4");
                listaEstatusPropiedad.Add("5"); listaEstatusPropiedad.Add("6");
                listaEstatusPropiedad.Add("N"); listaEstatusPropiedad.Add("");

                // Generar archivo plano
                //using (StreamWriter writer = new StreamWriter(stream))
                //{
                //    while (count <= max)
                //    {
                //        if (count == 1)
                //        {
                //            writer.WriteLine("{\"Consolidado\":\"40020\",\"Id_Formato\":\"1025\",\"Formato\":\"Fogafin\",\"Codigo_Entidad\":null,\"Entidad\":\"TIZZIANO_-_ACH\",\"Tipo_Entidad\":null,\"Sector\":null,\"Fecha_Creacion\":\"2020/07/2212:17:09\",\"Fecha_Modificacion\":\"2020/07/2212:44:09\",\"Usuario\":\"VolcadoToBe\",\"Estado_Volcado\":\"TFIN\",\"Error\":\"0\"}");
                //            count++;
                //        }                              
                        
                //        if (rnd.Next(40) < 5) //cada n registros se cree de nuevo la cabecera
                //        {
                //            //deserailizar a un objeto
                //            //luego envio el json el objeto
                //            //limpiar variables
                //            contador = 1;
                //            writer.WriteLine("{\"Consolidado\":\"40020\",\"Id_Formato\":\"1025\",\"Formato\":\"Fogafin\",\"Codigo_Entidad\":null,\"Entidad\":\"TIZZIANO_-_ACH\",\"Tipo_Entidad\":null,\"Sector\":null,\"Fecha_Creacion\":\"2020/07/2212:17:09\",\"Fecha_Modificacion\":\"2020/07/2212:44:09\",\"Usuario\":\"VolcadoToBe\",\"Estado_Volcado\":\"TFIN\",\"Error\":\"0\"}");
                //            count++;
                //        }

                //        writer.WriteLine("{\"Consolidado\":\"40020\",\"Entidad\":\"TIZZIANO_-_ACH\",\"Fecha\":\"2020/07/2212:44:07\",\"Usuario\":\"VolcadoToBe\",\"Registro_descripcion\":\"Fogafin_1025\",\"Registro_ordenador\":\"" + contador + "\",\"Registro_tipo\":\"Dato\",\"Campos\":[{\"Orden\":\"" + contador + "\",\"Campo_IdLog\":\"SECUENCIA\",\"Campo_Nombre\":\"SECUENCIA\",\"Campo_Valor\":\"17\",\"Campo_Posicion\":\"" + contador + ",0\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"2\",\"Campo_IdLog\":\"NIT\",\"Campo_Nombre\":\"NIT\",\"Campo_Valor\":\"" + listaNit[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",1\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"3\",\"Campo_IdLog\":\"DV\",\"Campo_Nombre\":\"DV\",\"Campo_Valor\":\"" + listaDV[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",2\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"4\",\"Campo_IdLog\":\"PRIMER_APELLIDO\",\"Campo_Nombre\":\"PRIMER_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(30)] + "\",\"Campo_Posicion\":\"" + contador + ",3\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"5\",\"Campo_IdLog\":\"SEGUNDO_APELLIDO\",\"Campo_Nombre\":\"SEGUNDO_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",4\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"6\",\"Campo_IdLog\":\"PRIMER_NOMBRE\",\"Campo_Nombre\":\"PRIMER_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",5\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"7\",\"Campo_IdLog\":\"SEGUNDO_NOMBRE\",\"Campo_Nombre\":\"SEGUNDO_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",6\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"8\",\"Campo_IdLog\":\"RAZON_SOCIAL\",\"Campo_Nombre\":\"RAZON_SOCIAL\",\"Campo_Valor\":\"" + listaRazonSocial[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",7\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"9\",\"Campo_IdLog\":\"TELEFONO\",\"Campo_Nombre\":\"TELEFONO\",\"Campo_Valor\":\"" + listaTelefono[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",8\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"10\",\"Campo_IdLog\":\"MAIL\",\"Campo_Nombre\":\"MAIL\",\"Campo_Valor\":\"" + listaMail[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",9\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"11\",\"Campo_IdLog\":\"TIPO_CLIENTE\",\"Campo_Nombre\":\"TIPO_CLIENTE\",\"Campo_Valor\":\"" + listaTipoCliente[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",10\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"12\",\"Campo_IdLog\":\"DIRECCION\",\"Campo_Nombre\":\"DIRECCION\",\"Campo_Valor\":\"" + listaDireccion[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",11\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"13\",\"Campo_IdLog\":\"TIPO_CLIENTE2\",\"Campo_Nombre\":\"TIPO_CLIENTE2\",\"Campo_Valor\":\"" + listaTipoCliente2[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",12\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"14\",\"Campo_IdLog\":\"TIPO_IDENTIFICACION\",\"Campo_Nombre\":\"TIPO_IDENTIFICACION\",\"Campo_Valor\":\"" + listaTipoIdent[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",13\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"15\",\"Campo_IdLog\":\"COD_DEPARTAMENTO\",\"Campo_Nombre\":\"COD_DEPARTAMENTO\",\"Campo_Valor\":\"" + listaCodDepa[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",14\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"16\",\"Campo_IdLog\":\"COD_MUNICIPIO\",\"Campo_Nombre\":\"COD_MUNICIPIO\",\"Campo_Valor\":\"" + listaCodeMuni[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",15\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"17\",\"Campo_IdLog\":\"CELULAR\",\"Campo_Nombre\":\"CELULAR\",\"Campo_Valor\":\"" + listaCelular[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",16\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"18\",\"Campo_IdLog\":\"TIPO_DEPOSITO\",\"Campo_Nombre\":\"TIPO_DEPOSITO\",\"Campo_Valor\":\"" + listaTipoDeposito[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",17\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"19\",\"Campo_IdLog\":\"CLASE_AHORRO\",\"Campo_Nombre\":\"CLASE_AHORRO\",\"Campo_Valor\":\"" + listaClaseAhorro[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",18\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"20\",\"Campo_IdLog\":\"NUMERO_PRODUCTO\",\"Campo_Nombre\":\"NUMERO_PRODUCTO\",\"Campo_Valor\":\"" + listaNumeroProd[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",19\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"21\",\"Campo_IdLog\":\"ESTADO_CAP\",\"Campo_Nombre\":\"ESTADO_CAP\",\"Campo_Valor\":\"" + listaEstadoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",20\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"22\",\"Campo_IdLog\":\"TIPO_DE_CUENTA\",\"Campo_Nombre\":\"TIPO_DE_CUENTA\",\"Campo_Valor\":\"" + listaTipoCuenta[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",21\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"23\",\"Campo_IdLog\":\"ID_TITULAR\",\"Campo_Nombre\":\"ID_TITULAR\",\"Campo_Valor\":\"" + listaIdTitular[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",22\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"24\",\"Campo_IdLog\":\"FECHA_SALDO_CAP\",\"Campo_Nombre\":\"FECHA_SALDO_CAP\",\"Campo_Valor\":\"" + listaFechaSaldoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",23\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"25\",\"Campo_IdLog\":\"SALDO_CAP\",\"Campo_Nombre\":\"SALDO_CAP\",\"Campo_Valor\":\"" + listaSaldoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",24\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"26\",\"Campo_IdLog\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Nombre\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Valor\":\"" + listaValorInteres[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",25\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"27\",\"Campo_IdLog\":\"CODIGO_MONEDA\",\"Campo_Nombre\":\"CODIGO_MONEDA\",\"Campo_Valor\":\"" + listaCodMoneda[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",26\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"28\",\"Campo_IdLog\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Nombre\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Valor\":\"" + listaEstatusPropiedad[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",27\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"}]}");
                        
                //        count++;
                //        contador++;
                //    }
                //}

                //Guaradar en MongoBD
                while (count <= max)
                {
                    if (count == 1)
                    {
                        jsonCabecera = "{\"Consolidado\":\"40020\",\"Id_Formato\":\"1025\",\"Formato\":\"Fogafin\",\"Codigo_Entidad\":null,\"Entidad\":\"TIZZIANO_-_ACH\",\"Tipo_Entidad\":null,\"Sector\":null,\"Fecha_Creacion\":\"2020/07/2212:17:09\",\"Fecha_Modificacion\":\"2020/07/2212:44:09\",\"Usuario\":\"VolcadoToBe\",\"Estado_Volcado\":\"TFIN\",\"Error\":\"0\"}";                        
                        count++;
                    }

                    if (contador == 1)
                    {
                        jsonDetalle = "[{\"Consolidad\":\"40020\",\"Entidad\":\"TIZZIANO_-_ACH\",\"Fecha\":\"2020/07/2212:44:07\",\"Usuario\":\"VolcadoToBe\",\"Registro_descripcion\":\"Fogafin_1025\",\"Registro_ordenador\":\"" + contador + "\",\"Registro_tipo\":\"Dato\",\"Campos\":[{\"Orden\":\"" + contador + "\",\"Campo_IdLog\":\"SECUENCIA\",\"Campo_Nombre\":\"SECUENCIA\",\"Campo_Valor\":\"17\",\"Campo_Posicion\":\"" + contador + ",0\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"2\",\"Campo_IdLog\":\"NIT\",\"Campo_Nombre\":\"NIT\",\"Campo_Valor\":\"" + listaNit[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",1\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"3\",\"Campo_IdLog\":\"DV\",\"Campo_Nombre\":\"DV\",\"Campo_Valor\":\"" + listaDV[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",2\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"4\",\"Campo_IdLog\":\"PRIMER_APELLIDO\",\"Campo_Nombre\":\"PRIMER_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(30)] + "\",\"Campo_Posicion\":\"" + contador + ",3\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"5\",\"Campo_IdLog\":\"SEGUNDO_APELLIDO\",\"Campo_Nombre\":\"SEGUNDO_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",4\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"6\",\"Campo_IdLog\":\"PRIMER_NOMBRE\",\"Campo_Nombre\":\"PRIMER_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",5\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"7\",\"Campo_IdLog\":\"SEGUNDO_NOMBRE\",\"Campo_Nombre\":\"SEGUNDO_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",6\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"8\",\"Campo_IdLog\":\"RAZON_SOCIAL\",\"Campo_Nombre\":\"RAZON_SOCIAL\",\"Campo_Valor\":\"" + listaRazonSocial[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",7\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"9\",\"Campo_IdLog\":\"TELEFONO\",\"Campo_Nombre\":\"TELEFONO\",\"Campo_Valor\":\"" + listaTelefono[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",8\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"10\",\"Campo_IdLog\":\"MAIL\",\"Campo_Nombre\":\"MAIL\",\"Campo_Valor\":\"" + listaMail[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",9\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"11\",\"Campo_IdLog\":\"TIPO_CLIENTE\",\"Campo_Nombre\":\"TIPO_CLIENTE\",\"Campo_Valor\":\"" + listaTipoCliente[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",10\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"12\",\"Campo_IdLog\":\"DIRECCION\",\"Campo_Nombre\":\"DIRECCION\",\"Campo_Valor\":\"" + listaDireccion[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",11\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"13\",\"Campo_IdLog\":\"TIPO_CLIENTE2\",\"Campo_Nombre\":\"TIPO_CLIENTE2\",\"Campo_Valor\":\"" + listaTipoCliente2[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",12\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"14\",\"Campo_IdLog\":\"TIPO_IDENTIFICACION\",\"Campo_Nombre\":\"TIPO_IDENTIFICACION\",\"Campo_Valor\":\"" + listaTipoIdent[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",13\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"15\",\"Campo_IdLog\":\"COD_DEPARTAMENTO\",\"Campo_Nombre\":\"COD_DEPARTAMENTO\",\"Campo_Valor\":\"" + listaCodDepa[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",14\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"16\",\"Campo_IdLog\":\"COD_MUNICIPIO\",\"Campo_Nombre\":\"COD_MUNICIPIO\",\"Campo_Valor\":\"" + listaCodeMuni[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",15\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"17\",\"Campo_IdLog\":\"CELULAR\",\"Campo_Nombre\":\"CELULAR\",\"Campo_Valor\":\"" + listaCelular[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",16\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"18\",\"Campo_IdLog\":\"TIPO_DEPOSITO\",\"Campo_Nombre\":\"TIPO_DEPOSITO\",\"Campo_Valor\":\"" + listaTipoDeposito[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",17\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"19\",\"Campo_IdLog\":\"CLASE_AHORRO\",\"Campo_Nombre\":\"CLASE_AHORRO\",\"Campo_Valor\":\"" + listaClaseAhorro[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",18\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"20\",\"Campo_IdLog\":\"NUMERO_PRODUCTO\",\"Campo_Nombre\":\"NUMERO_PRODUCTO\",\"Campo_Valor\":\"" + listaNumeroProd[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",19\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"21\",\"Campo_IdLog\":\"ESTADO_CAP\",\"Campo_Nombre\":\"ESTADO_CAP\",\"Campo_Valor\":\"" + listaEstadoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",20\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"22\",\"Campo_IdLog\":\"TIPO_DE_CUENTA\",\"Campo_Nombre\":\"TIPO_DE_CUENTA\",\"Campo_Valor\":\"" + listaTipoCuenta[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",21\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"23\",\"Campo_IdLog\":\"ID_TITULAR\",\"Campo_Nombre\":\"ID_TITULAR\",\"Campo_Valor\":\"" + listaIdTitular[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",22\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"24\",\"Campo_IdLog\":\"FECHA_SALDO_CAP\",\"Campo_Nombre\":\"FECHA_SALDO_CAP\",\"Campo_Valor\":\"" + listaFechaSaldoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",23\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"25\",\"Campo_IdLog\":\"SALDO_CAP\",\"Campo_Nombre\":\"SALDO_CAP\",\"Campo_Valor\":\"" + listaSaldoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",24\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"26\",\"Campo_IdLog\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Nombre\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Valor\":\"" + listaValorInteres[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",25\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"27\",\"Campo_IdLog\":\"CODIGO_MONEDA\",\"Campo_Nombre\":\"CODIGO_MONEDA\",\"Campo_Valor\":\"" + listaCodMoneda[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",26\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"28\",\"Campo_IdLog\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Nombre\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Valor\":\"" + listaEstatusPropiedad[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",27\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"}]}";
                    }
                    else
                    {
                        jsonDetalle += ",{\"Consolidad\":\"40020\",\"Entidad\":\"TIZZIANO_-_ACH\",\"Fecha\":\"2020/07/2212:44:07\",\"Usuario\":\"VolcadoToBe\",\"Registro_descripcion\":\"Fogafin_1025\",\"Registro_ordenador\":\"" + contador + "\",\"Registro_tipo\":\"Dato\",\"Campos\":[{\"Orden\":\"" + contador + "\",\"Campo_IdLog\":\"SECUENCIA\",\"Campo_Nombre\":\"SECUENCIA\",\"Campo_Valor\":\"17\",\"Campo_Posicion\":\"" + contador + ",0\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"2\",\"Campo_IdLog\":\"NIT\",\"Campo_Nombre\":\"NIT\",\"Campo_Valor\":\"" + listaNit[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",1\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"3\",\"Campo_IdLog\":\"DV\",\"Campo_Nombre\":\"DV\",\"Campo_Valor\":\"" + listaDV[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",2\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"4\",\"Campo_IdLog\":\"PRIMER_APELLIDO\",\"Campo_Nombre\":\"PRIMER_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(30)] + "\",\"Campo_Posicion\":\"" + contador + ",3\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"5\",\"Campo_IdLog\":\"SEGUNDO_APELLIDO\",\"Campo_Nombre\":\"SEGUNDO_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",4\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"6\",\"Campo_IdLog\":\"PRIMER_NOMBRE\",\"Campo_Nombre\":\"PRIMER_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",5\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"7\",\"Campo_IdLog\":\"SEGUNDO_NOMBRE\",\"Campo_Nombre\":\"SEGUNDO_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(7)] + "\",\"Campo_Posicion\":\"" + contador + ",6\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"8\",\"Campo_IdLog\":\"RAZON_SOCIAL\",\"Campo_Nombre\":\"RAZON_SOCIAL\",\"Campo_Valor\":\"" + listaRazonSocial[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",7\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"9\",\"Campo_IdLog\":\"TELEFONO\",\"Campo_Nombre\":\"TELEFONO\",\"Campo_Valor\":\"" + listaTelefono[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",8\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"10\",\"Campo_IdLog\":\"MAIL\",\"Campo_Nombre\":\"MAIL\",\"Campo_Valor\":\"" + listaMail[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",9\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"11\",\"Campo_IdLog\":\"TIPO_CLIENTE\",\"Campo_Nombre\":\"TIPO_CLIENTE\",\"Campo_Valor\":\"" + listaTipoCliente[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",10\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"12\",\"Campo_IdLog\":\"DIRECCION\",\"Campo_Nombre\":\"DIRECCION\",\"Campo_Valor\":\"" + listaDireccion[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",11\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"13\",\"Campo_IdLog\":\"TIPO_CLIENTE2\",\"Campo_Nombre\":\"TIPO_CLIENTE2\",\"Campo_Valor\":\"" + listaTipoCliente2[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",12\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"14\",\"Campo_IdLog\":\"TIPO_IDENTIFICACION\",\"Campo_Nombre\":\"TIPO_IDENTIFICACION\",\"Campo_Valor\":\"" + listaTipoIdent[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",13\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"15\",\"Campo_IdLog\":\"COD_DEPARTAMENTO\",\"Campo_Nombre\":\"COD_DEPARTAMENTO\",\"Campo_Valor\":\"" + listaCodDepa[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",14\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"16\",\"Campo_IdLog\":\"COD_MUNICIPIO\",\"Campo_Nombre\":\"COD_MUNICIPIO\",\"Campo_Valor\":\"" + listaCodeMuni[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",15\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"17\",\"Campo_IdLog\":\"CELULAR\",\"Campo_Nombre\":\"CELULAR\",\"Campo_Valor\":\"" + listaCelular[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",16\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"18\",\"Campo_IdLog\":\"TIPO_DEPOSITO\",\"Campo_Nombre\":\"TIPO_DEPOSITO\",\"Campo_Valor\":\"" + listaTipoDeposito[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",17\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"19\",\"Campo_IdLog\":\"CLASE_AHORRO\",\"Campo_Nombre\":\"CLASE_AHORRO\",\"Campo_Valor\":\"" + listaClaseAhorro[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",18\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"20\",\"Campo_IdLog\":\"NUMERO_PRODUCTO\",\"Campo_Nombre\":\"NUMERO_PRODUCTO\",\"Campo_Valor\":\"" + listaNumeroProd[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",19\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"21\",\"Campo_IdLog\":\"ESTADO_CAP\",\"Campo_Nombre\":\"ESTADO_CAP\",\"Campo_Valor\":\"" + listaEstadoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",20\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"22\",\"Campo_IdLog\":\"TIPO_DE_CUENTA\",\"Campo_Nombre\":\"TIPO_DE_CUENTA\",\"Campo_Valor\":\"" + listaTipoCuenta[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",21\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"23\",\"Campo_IdLog\":\"ID_TITULAR\",\"Campo_Nombre\":\"ID_TITULAR\",\"Campo_Valor\":\"" + listaIdTitular[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",22\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"24\",\"Campo_IdLog\":\"FECHA_SALDO_CAP\",\"Campo_Nombre\":\"FECHA_SALDO_CAP\",\"Campo_Valor\":\"" + listaFechaSaldoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",23\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"25\",\"Campo_IdLog\":\"SALDO_CAP\",\"Campo_Nombre\":\"SALDO_CAP\",\"Campo_Valor\":\"" + listaSaldoCap[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",24\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"26\",\"Campo_IdLog\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Nombre\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Valor\":\"" + listaValorInteres[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",25\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"27\",\"Campo_IdLog\":\"CODIGO_MONEDA\",\"Campo_Nombre\":\"CODIGO_MONEDA\",\"Campo_Valor\":\"" + listaCodMoneda[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",26\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"},{\"Orden\":\"28\",\"Campo_IdLog\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Nombre\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Valor\":\"" + listaEstatusPropiedad[rnd.Next(3)] + "\",\"Campo_Posicion\":\"" + contador + ",27\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"\",\"Campo_Vert\":\"\"}]}";
                    }

                    if (rnd.Next(40) < 4) //cada n registros se cree de nuevo la cabecera
                    {
                        contador = 0;
                        //deserailizar a un objeto
                        jsonDetalle += "]";
                        ConsolidadoEncabezado CabeceraObjeto = json_serializer.Deserialize<ConsolidadoEncabezado>(jsonCabecera);
                        List<FormatoToBe> DetalleObjeto = json_serializer.Deserialize<List<FormatoToBe>>(jsonDetalle);
                        //luego envio el json el objeto
                        eGenerarFormato.EnviarDatosToBe(CabeceraObjeto, DetalleObjeto);
                        //limpiar variables
                        jsonDetalle = "";
                        jsonCabecera = "";
                        jsonCabecera = "{\"Consolidado\":\"40020\",\"Id_Formato\":\"1025\",\"Formato\":\"Fogafin\",\"Codigo_Entidad\":null,\"Entidad\":\"TIZZIANO_-_ACH\",\"Tipo_Entidad\":null,\"Sector\":null,\"Fecha_Creacion\":\"2020/07/2212:17:09\",\"Fecha_Modificacion\":\"2020/07/2212:44:09\",\"Usuario\":\"VolcadoToBe\",\"Estado_Volcado\":\"TFIN\",\"Error\":\"0\"}";
                        count++;
                    }                   

                    count++;
                    contador++;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
