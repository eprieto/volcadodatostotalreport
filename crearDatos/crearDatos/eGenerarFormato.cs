﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crearDatos
{
    public class eGenerarFormato
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;

        public eGenerarFormato()
        {
                
        }

        ~eGenerarFormato()
        {

        }

        /// <summary>
        /// Genera la estructura de la cabecera y envia datos para el detalle
        /// </summary>
        /// <param name="encabezado"></param>
        /// <param name="Detalle"></param>
        /// <returns></returns>
        public static bool EnviarDatosToBe(ConsolidadoEncabezado encabezado, List<FormatoToBe> Detalle) 
        {
            bool result = true;
            try
            {
                ConsolidadoEncabezado consolidadoEncabezado = new ConsolidadoEncabezado()
                {
                    Consolidado = encabezado.Consolidado,
                    Id_Formato = encabezado.Id_Formato,
                    Formato = encabezado.Formato,
                    Codigo_Entidad = encabezado.Codigo_Entidad,
                    Entidad = encabezado.Entidad,
                    Tipo_Entidad = encabezado.Tipo_Entidad,
                    Sector = encabezado.Sector,
                    Fecha_Creacion = encabezado.Fecha_Creacion,
                    Fecha_Modificacion = encabezado.Fecha_Modificacion,
                    Usuario = encabezado.Usuario,
                    Estado_Volcado = encabezado.Estado_Volcado,
                    Error = encabezado.Error
                };

                EncabezadoToBe(consolidadoEncabezado);

                CrearEstructuraJsonToBe(Detalle);

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                result = false;
                ex.ToString();
            }
            return result;

        }

        /// <summary>
        /// Genera la estrucutra del cuerpo
        /// </summary>
        /// <param name="Detalle"></param>
        /// <returns></returns>
        public static bool CrearEstructuraJsonToBe(List<FormatoToBe> Detalle) {

            bool result = true;
            try
            {
                foreach (var dato in Detalle)
                {
                    FormatoToBe Registro = new FormatoToBe
                    {
                        Consolidado = dato.Consolidado,
                        Entidad = dato.Entidad,
                        Fecha = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                        Usuario = dato.Usuario,
                        Registro_Descripcion = dato.Registro_Descripcion,
                        Registro_Ordenador = dato.Registro_Ordenador,
                        Registro_Tipo = dato.Registro_Tipo,
                        Campos = dato.Campos
                    };
                    DetalleToBe(Registro);
                }                     
            }
            catch (Exception ex)
            {
                result = false;
                ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// Guarda en la BD la cabecera 
        /// </summary>
        /// <param name="consolidadoEncabezado"></param>
        /// <returns></returns>
        public static bool EncabezadoToBe(ConsolidadoEncabezado consolidadoEncabezado) 
        {
            bool result = true;
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["MongoConnectionStr"].ConnectionString;
                var dataBase = ConfigurationManager.AppSettings["MongoDataBase"];
                _client = new MongoClient(connectionString);
                _database = _client.GetDatabase(dataBase);

                var collection = _database.GetCollection<BsonDocument>("db_consolidado_encabezado");

                var bsonDocument = consolidadoEncabezado.ToBsonDocument();
                collection.InsertOneAsync(bsonDocument);
            }
            catch (Exception ex)
            {
                result = false;
                ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// Guarda en la BD el detalle
        /// </summary>
        /// <param name="consolidadoDetalle"></param>
        /// <returns></returns>
        public static bool DetalleToBe(FormatoToBe consolidadoDetalle)
        {
            bool result = true;
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["MongoConnectionStr"].ConnectionString;
                var dataBase = ConfigurationManager.AppSettings["MongoDataBase"];
                _client = new MongoClient(connectionString);
                _database = _client.GetDatabase(dataBase);

                var collection = _database.GetCollection<BsonDocument>("db_consolidado_detalle");

                var bsonDocument = consolidadoDetalle.ToBsonDocument();
                collection.InsertOneAsync(bsonDocument);
            }
            catch (Exception ex)
            {
                result = false;
                ex.ToString();
            }
            return result;
        }

    }
}
