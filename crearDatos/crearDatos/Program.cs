﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace crearDatos
{
    class Program
    {/*
        static void Main(string[] args)
        {
            string path = @"C:\\Temp\Datos_Vol_Fogafin.txt";
            Random rnd = new Random();

            try
            {
                ArrayList listaNombres = new ArrayList();
                listaNombres.Add("DIEGO"); listaNombres.Add("FERNANDO");
                listaNombres.Add("CARLOS"); listaNombres.Add("ANDRES");
                listaNombres.Add("LUIS"); listaNombres.Add("EDWIN");
                listaNombres.Add("FRANK"); listaNombres.Add("NICOLAS");
                listaNombres.Add("ALEJANDRO"); listaNombres.Add("TOMAS");
                listaNombres.Add("JUAN"); listaNombres.Add("CAMILO");
                listaNombres.Add("PEDRO"); listaNombres.Add("PAUBLO");
                listaNombres.Add("MATEO"); listaNombres.Add("ELIZABET");
                listaNombres.Add("ANDREA"); listaNombres.Add("CAROLINA");
                listaNombres.Add("INGRID"); listaNombres.Add("LIZETH");
                listaNombres.Add("LAURA"); listaNombres.Add("SILVIA");
                listaNombres.Add("ALBERTO"); listaNombres.Add("JAVIER");
                listaNombres.Add("VICTOR"); listaNombres.Add("SEBASTIAN");
                listaNombres.Add("DIANA"); listaNombres.Add("SANDRA");
                listaNombres.Add("JHONATAN"); listaNombres.Add("PAOLA");
                listaNombres.Add("JENNIFER"); listaNombres.Add("DANIEL");
                listaNombres.Add("EDUARDO"); listaNombres.Add("EDGAR");
                listaNombres.Add("SOFIA"); listaNombres.Add("RAUL");
                listaNombres.Add("MARIA"); listaNombres.Add("JULIAN");
                listaNombres.Add("JHOAN"); listaNombres.Add("JOSE");
                listaNombres.Add("NATHALIA"); listaNombres.Add("VIVIANA");
                listaNombres.Add("GABRIEL"); listaNombres.Add("JARRY");
                listaNombres.Add("FABIAN"); listaNombres.Add("MAURICIO");
                listaNombres.Add("TATIANA"); listaNombres.Add("MARTA");
                listaNombres.Add("ERIKA"); listaNombres.Add("JULY");
                listaNombres.Add("PATRICIA"); listaNombres.Add("CAMILA");
                listaNombres.Add("LILIANA"); listaNombres.Add("CLAUDIA");
                listaNombres.Add("JORGE"); listaNombres.Add("JEFERSON");
                listaNombres.Add("ROMAN"); listaNombres.Add("JEISON");
                listaNombres.Add("CINDY"); listaNombres.Add("MARTIN");

                ArrayList listaApellidos = new ArrayList();
                FileStream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);

                listaApellidos.Add("SOLER"); listaApellidos.Add("FLOREZ");
                listaApellidos.Add("RODRIGUEZ"); listaApellidos.Add("GOMEZ");
                listaApellidos.Add("LOPEZ"); listaApellidos.Add("CONZALEZ");
                listaApellidos.Add("GARCIA"); listaApellidos.Add("MARTINEZ");
                listaApellidos.Add("RAMIREZ"); listaApellidos.Add("SANCHEZ");
                listaApellidos.Add("PREREZ"); listaApellidos.Add("MEJIA");
                listaApellidos.Add("HERNANDEZ"); listaApellidos.Add("DIAZ");
                listaApellidos.Add("TORRES"); listaApellidos.Add("ROJAS");
                listaApellidos.Add("VARGAZ"); listaApellidos.Add("MORENO");
                listaApellidos.Add("GUTIERREZ"); listaApellidos.Add("CASTRO");
                listaApellidos.Add("MUÑOZ"); listaApellidos.Add("JIMENEZ");
                listaApellidos.Add("ORTIZ"); listaApellidos.Add("ALVAREZ");
                listaApellidos.Add("RUIZ"); listaApellidos.Add("SUAREZ");
                listaApellidos.Add("ROMERO"); listaApellidos.Add("SALAZAR");
                listaApellidos.Add("HERRERA"); listaApellidos.Add("VALENCIA");
                listaApellidos.Add("QUINTERO"); listaApellidos.Add("RESTREPO");
                listaApellidos.Add("MOLALES"); listaApellidos.Add("MEJIA");
                listaApellidos.Add("ARIAS"); listaApellidos.Add("PARRA");
                listaApellidos.Add("CARDENAS"); listaApellidos.Add("OSOSRIO");
                listaApellidos.Add("CASTILLO"); listaApellidos.Add("CARDONA");
                listaApellidos.Add("MEDINA"); listaApellidos.Add("RIVERA");
                listaApellidos.Add("CORTES"); listaApellidos.Add("CORREA");
                listaApellidos.Add("MARIN"); listaApellidos.Add("RINCON");
                listaApellidos.Add("ZAPATA"); listaApellidos.Add("ESCOBAR");
                listaApellidos.Add("URIBE"); listaApellidos.Add("SAMPER");
                listaApellidos.Add("DUARTE"); listaApellidos.Add("MARTINEZ");
                listaApellidos.Add("FUENTES"); listaApellidos.Add("ROJAS");
                listaApellidos.Add("MOLINA"); listaApellidos.Add("LUNA");
                listaApellidos.Add("NAVARRO"); listaApellidos.Add("MENDOZA");
                listaApellidos.Add("CAMPOS"); listaApellidos.Add("SOTO");

                int n = 1;

                int idtitulas = 1;
                int idtitulasCop = 2;

                int max = 1000000;
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    while (n <= max)
                    {
                        writer.WriteLine("[{\"Consolidado\":\"1\",\"Entidad\":\"Fogafin\",\"Fecha\":\"02/06/2020\",\"Usuario\":\"lrangel\",\"Registro_descripcion\":\"Registro_Ejemplo\",\"Registro_ordenador\":\""+n+"\",\"Registro_tipo\":\"Datos\",\"Campos\":[{\"Orden\":\"1\",\"Campo_IdLog\":\"SECUENCIA\",\"Campo_Nombre\":\"SECUENCIA\",\"Campo_Valor\":\""+ n +"\",\"Campo_Posicion\":\""+n+",1\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"1\"},{\"Orden\":\"2\",\"Campo_IdLog\":\"NIT\",\"Campo_Nombre\":\"NIT\",\"Campo_Valor\":\""+ ( 1098637848 + n )+ "\",\"Campo_Posicion\":\"" + n + ",2\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"2\"},{\"Orden\":\"3\",\"Campo_IdLog\":\"DV\",\"Campo_Nombre\":\"DV\",\"Campo_Valor\":\"N\",\"Campo_Posicion\":\"" + n + ",3\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"3\"},{\"Orden\":\"4\",\"Campo_IdLog\":\"PRIMER_NOMBRE\",\"Campo_Nombre\":\"PRIMER_NOMBRE\",\"Campo_Valor\":\""+listaNombres[rnd.Next(59)]+"\",\"Campo_Posicion\":\"" + n + ",4\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"4\"},{\"Orden\":\"5\",\"Campo_IdLog\":\"SEGUNDO_NOMBRE\",\"Campo_Nombre\":\"SEGUNDO_NOMBRE\",\"Campo_Valor\":\"\",\"Campo_Posicion\":\"" + n + ",5\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+ "\",\"Campo_Vert\":\"5\"},{\"Orden\":\"6\",\"Campo_IdLog\":\"PRIMER_APELLIDO\",\"Campo_Nombre\":\"PRIMER_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(59)] + "\",\"Campo_Posicion\":\"" + n + ",6\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+ "\",\"Campo_Vert\":\"6\"},{\"Orden\":\"7\",\"Campo_IdLog\":\"SEGUNDO_APELLIDO\",\"Campo_Nombre\":\"SEGUNDO_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(59)] + "\",\"Campo_Posicion\":\"" + n + ",7\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"7\"},{\"Orden\":\"8\",\"Campo_IdLog\":\"RAZON_SOCIAL\",\"Campo_Nombre\":\"RAZON_SOCIAL\",\"Campo_Valor\":\"\",\"Campo_Posicion\":\"" + n + ",8\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"8\"},{\"Orden\":\"9\",\"Campo_IdLog\":\"TELEFONO\",\"Campo_Nombre\":\"TELEFONO\",\"Campo_Valor\":\"8366091\",\"Campo_Posicion\":\"" + n + ",9\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"9\"},{\"Orden\":\"10\",\"Campo_IdLog\":\"MAIL\",\"Campo_Nombre\":\"MAIL\",\"Campo_Valor\":\"ELEJEMPLO" + n+ "@JWPROJECT.COM\",\"Campo_Posicion\":\"" + n + ",10\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"10\"},{\"Orden\":\"11\",\"Campo_IdLog\":\"TIPO_CLIENTE\",\"Campo_Nombre\":\"TIPO_CLIENTE\",\"Campo_Valor\":\"3\",\"Campo_Posicion\":\"" + n + ",11\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"11\"},{\"Orden\":\"12\",\"Campo_IdLog\":\"DIRECCIÓN\",\"Campo_Nombre\":\"DIRECCIÓN\",\"Campo_Valor\":\"CL17NO839\",\"Campo_Posicion\":\"" + n + ",12\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"12\"},{\"Orden\":\"13\",\"Campo_IdLog\":\"TIPO_CLIENTE2\",\"Campo_Nombre\":\"TIPO_CLIENTE2\",\"Campo_Valor\":\"NA\",\"Campo_Posicion\":\"" + n + ",13\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"13\"},{\"Orden\":\"14\",\"Campo_IdLog\":\"TIPO_IDENTIFICACION\",\"Campo_Nombre\":\"TIPO_IDENTIFICACION\",\"Campo_Valor\":\"1\",\"Campo_Posicion\":\"" + n + ",14\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"14\"},{\"Orden\":\"15\",\"Campo_IdLog\":\"COD_DEPARTAMENTO\",\"Campo_Nombre\":\"COD_DEPARTAMENTO\",\"Campo_Valor\":\"25\",\"Campo_Posicion\":\"" + n + ",15\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"15\"},{\"Orden\":\"16\",\"Campo_IdLog\":\"COD_MUNICIPIO\",\"Campo_Nombre\":\"COD_MUNICIPIO\",\"Campo_Valor\":\"40\",\"Campo_Posicion\":\"" + n + ",16\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"16\"},{\"Orden\":\"17\",\"Campo_IdLog\":\"CELULAR\",\"Campo_Nombre\":\"CELULAR\",\"Campo_Valor\":\""+(3014838820+n)+"\",\"Campo_Posicion\":\"" + n + ",17\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"17\"},{\"Orden\":\"18\",\"Campo_IdLog\":\"TIPO_DEPOSITO\",\"Campo_Nombre\":\"TIPO_DEPOSITO\",\"Campo_Valor\":\"7\",\"Campo_Posicion\":\"" + n + ",18\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"18\"},{\"Orden\":\"19\",\"Campo_IdLog\":\"CLASE_AHORRO\",\"Campo_Nombre\":\"CLASE_AHORRO\",\"Campo_Valor\":\"1\",\"Campo_Posicion\":\"" + n + ",19\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"19\"},{\"Orden\":\"20\",\"Campo_IdLog\":\"NUMERO_PRODUCTO\",\"Campo_Nombre\":\"NUMERO_PRODUCTO\",\"Campo_Valor\":\""+(40010202010+n)+"\",\"Campo_Posicion\":\"" + n + ",20\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"20\"},{\"Orden\":\"21\",\"Campo_IdLog\":\"ESTADO_CAP\",\"Campo_Nombre\":\"ESTADO_CAP\",\"Campo_Valor\":\"0\",\"Campo_Posicion\":\"" + n + ",21\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+ "\",\"Campo_Vert\":\"21\"},{\"Orden\":\"22\",\"Campo_IdLog\":\"TIPO_DE_CUENTA\",\"Campo_Nombre\":\"TIPO_DE_CUENTA\",\"Campo_Valor\":\"" + 1 + "\",\"Campo_Posicion\":\"" + n + ",22\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"22\"},{\"Orden\":\"23\",\"Campo_IdLog\":\"ID_TITULAR\",\"Campo_Nombre\":\"ID_TITULAR\",\"Campo_Valor\":\""+ idtitulas + "\",\"Campo_Posicion\":\"" + n + ",23\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"23\"},{\"Orden\":\"24\",\"Campo_IdLog\":\"FECHA_SALDO_CAP\",\"Campo_Nombre\":\"FECHA_SALDO_CAP\",\"Campo_Valor\":\"26/05/14305\",\"Campo_Posicion\":\"" + n + ",24\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"24\"},{\"Orden\":\"25\",\"Campo_IdLog\":\"SALDO_CAP\",\"Campo_Nombre\":\"SALDO_CAP\",\"Campo_Valor\":\"122286287\",\"Campo_Posicion\":\"" + n + ",25\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"25\"},{\"Orden\":\"26\",\"Campo_IdLog\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Nombre\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Valor\":\"835835,981021972\",\"Campo_Posicion\":\"" + n + ",26\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+"\",\"Campo_Vert\":\"26\"},{\"Orden\":\"27\",\"Campo_IdLog\":\"CODIGO_MONEDA\",\"Campo_Nombre\":\"CODIGO_MONEDA\",\"Campo_Valor\":\"1\",\"Campo_Posicion\":\"" + n + ",27\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\""+n+ "\",\"Campo_Vert\":\"27\"},{\"Orden\":\"28\",\"Campo_IdLog\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Nombre\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Valor\":\"5\",\"Campo_Posicion\":\"" + n + ",28\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n+"\",\"Campo_Vert\":\"28\"}]}]");

                        if (rnd.Next(99)>90)
                        {
                            n++;
                            
                            writer.WriteLine("[{\"Consolidado\":\"1\",\"Entidad\":\"Fogafin\",\"Fecha\":\"02/06/2020\",\"Usuario\":\"lrangel\",\"Registro_descripcion\":\"Registro_Ejemplo\",\"Registro_ordenador\":\"" + n + "\",\"Registro_tipo\":\"Datos\",\"Campos\":[{\"Orden\":\"1\",\"Campo_IdLog\":\"SECUENCIA\",\"Campo_Nombre\":\"SECUENCIA\",\"Campo_Valor\":\"" + n + "\",\"Campo_Posicion\":\"" + n + ",1\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"1\"},{\"Orden\":\"2\",\"Campo_IdLog\":\"NIT\",\"Campo_Nombre\":\"NIT\",\"Campo_Valor\":\"" + (1098637848 + n) + "\",\"Campo_Posicion\":\"" + n + ",2\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"2\"},{\"Orden\":\"3\",\"Campo_IdLog\":\"DV\",\"Campo_Nombre\":\"DV\",\"Campo_Valor\":\"N\",\"Campo_Posicion\":\"" + n + ",3\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"3\"},{\"Orden\":\"4\",\"Campo_IdLog\":\"PRIMER_NOMBRE\",\"Campo_Nombre\":\"PRIMER_NOMBRE\",\"Campo_Valor\":\"" + listaNombres[rnd.Next(59)] + "\",\"Campo_Posicion\":\"" + n + ",4\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"4\"},{\"Orden\":\"5\",\"Campo_IdLog\":\"SEGUNDO_NOMBRE\",\"Campo_Nombre\":\"SEGUNDO_NOMBRE\",\"Campo_Valor\":\"\",\"Campo_Posicion\":\"" + n + ",5\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"5\"},{\"Orden\":\"6\",\"Campo_IdLog\":\"PRIMER_APELLIDO\",\"Campo_Nombre\":\"PRIMER_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(59)] + "\",\"Campo_Posicion\":\"" + n + ",6\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"6\"},{\"Orden\":\"7\",\"Campo_IdLog\":\"SEGUNDO_APELLIDO\",\"Campo_Nombre\":\"SEGUNDO_APELLIDO\",\"Campo_Valor\":\"" + listaApellidos[rnd.Next(59)] + "\",\"Campo_Posicion\":\"" + n + ",7\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"7\"},{\"Orden\":\"8\",\"Campo_IdLog\":\"RAZON_SOCIAL\",\"Campo_Nombre\":\"RAZON_SOCIAL\",\"Campo_Valor\":\"\",\"Campo_Posicion\":\"" + n + ",8\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"8\"},{\"Orden\":\"9\",\"Campo_IdLog\":\"TELEFONO\",\"Campo_Nombre\":\"TELEFONO\",\"Campo_Valor\":\"8366091\",\"Campo_Posicion\":\"" + n + ",9\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"9\"},{\"Orden\":\"10\",\"Campo_IdLog\":\"MAIL\",\"Campo_Nombre\":\"MAIL\",\"Campo_Valor\":\"ELEJEMPLO" + n + "@JWPROJECT.COM\",\"Campo_Posicion\":\"" + n + ",10\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"10\"},{\"Orden\":\"11\",\"Campo_IdLog\":\"TIPO_CLIENTE\",\"Campo_Nombre\":\"TIPO_CLIENTE\",\"Campo_Valor\":\"3\",\"Campo_Posicion\":\"" + n + ",11\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"11\"},{\"Orden\":\"12\",\"Campo_IdLog\":\"DIRECCIÓN\",\"Campo_Nombre\":\"DIRECCIÓN\",\"Campo_Valor\":\"CL17NO839\",\"Campo_Posicion\":\"" + n + ",12\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"12\"},{\"Orden\":\"13\",\"Campo_IdLog\":\"TIPO_CLIENTE2\",\"Campo_Nombre\":\"TIPO_CLIENTE2\",\"Campo_Valor\":\"NA\",\"Campo_Posicion\":\"" + n + ",13\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"13\"},{\"Orden\":\"14\",\"Campo_IdLog\":\"TIPO_IDENTIFICACION\",\"Campo_Nombre\":\"TIPO_IDENTIFICACION\",\"Campo_Valor\":\"1\",\"Campo_Posicion\":\"" + n + ",14\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"14\"},{\"Orden\":\"15\",\"Campo_IdLog\":\"COD_DEPARTAMENTO\",\"Campo_Nombre\":\"COD_DEPARTAMENTO\",\"Campo_Valor\":\"25\",\"Campo_Posicion\":\"" + n + ",15\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"15\"},{\"Orden\":\"16\",\"Campo_IdLog\":\"COD_MUNICIPIO\",\"Campo_Nombre\":\"COD_MUNICIPIO\",\"Campo_Valor\":\"40\",\"Campo_Posicion\":\"" + n + ",16\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"16\"},{\"Orden\":\"17\",\"Campo_IdLog\":\"CELULAR\",\"Campo_Nombre\":\"CELULAR\",\"Campo_Valor\":\"" + (3014838820 + n) + "\",\"Campo_Posicion\":\"" + n + ",17\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"17\"},{\"Orden\":\"18\",\"Campo_IdLog\":\"TIPO_DEPOSITO\",\"Campo_Nombre\":\"TIPO_DEPOSITO\",\"Campo_Valor\":\"7\",\"Campo_Posicion\":\"" + n + ",18\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"18\"},{\"Orden\":\"19\",\"Campo_IdLog\":\"CLASE_AHORRO\",\"Campo_Nombre\":\"CLASE_AHORRO\",\"Campo_Valor\":\"1\",\"Campo_Posicion\":\"" + n + ",19\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"19\"},{\"Orden\":\"20\",\"Campo_IdLog\":\"NUMERO_PRODUCTO\",\"Campo_Nombre\":\"NUMERO_PRODUCTO\",\"Campo_Valor\":\"" + (40010202010 + n -1) + "\",\"Campo_Posicion\":\"" + n + ",20\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"20\"},{\"Orden\":\"21\",\"Campo_IdLog\":\"ESTADO_CAP\",\"Campo_Nombre\":\"ESTADO_CAP\",\"Campo_Valor\":\"0\",\"Campo_Posicion\":\"" + n + ",21\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"21\"},{\"Orden\":\"22\",\"Campo_IdLog\":\"TIPO_DE_CUENTA\",\"Campo_Nombre\":\"TIPO_DE_CUENTA\",\"Campo_Valor\":\"0\",\"Campo_Posicion\":\"" + n + ",22\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"22\"},{\"Orden\":\"23\",\"Campo_IdLog\":\"ID_TITULAR\",\"Campo_Nombre\":\"ID_TITULAR\",\"Campo_Valor\":\"" + idtitulasCop + "\",\"Campo_Posicion\":\"" + n + ",23\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"23\"},{\"Orden\":\"24\",\"Campo_IdLog\":\"FECHA_SALDO_CAP\",\"Campo_Nombre\":\"FECHA_SALDO_CAP\",\"Campo_Valor\":\"26/05/14305\",\"Campo_Posicion\":\"" + n + ",24\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"24\"},{\"Orden\":\"25\",\"Campo_IdLog\":\"SALDO_CAP\",\"Campo_Nombre\":\"SALDO_CAP\",\"Campo_Valor\":\"122286287\",\"Campo_Posicion\":\"" + n + ",25\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"25\"},{\"Orden\":\"26\",\"Campo_IdLog\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Nombre\":\"VLR_INTERESES_POR_PAGAR\",\"Campo_Valor\":\"835835,981021972\",\"Campo_Posicion\":\"" + n + ",26\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"26\"},{\"Orden\":\"27\",\"Campo_IdLog\":\"CODIGO_MONEDA\",\"Campo_Nombre\":\"CODIGO_MONEDA\",\"Campo_Valor\":\"1\",\"Campo_Posicion\":\"" + n + ",27\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"27\"},{\"Orden\":\"28\",\"Campo_IdLog\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Nombre\":\"ESTATUS_DE_PROPIEDAD\",\"Campo_Valor\":\"5\",\"Campo_Posicion\":\"" + n + ",28\",\"Campo_Funcion\":\"\",\"Campo_Horiz\":\"" + n + "\",\"Campo_Vert\":\"28\"}]}]");
                        }

                        n++;
                    }
                    
                }

            }
            catch (Exception e)
            {
                
            }

            
        }
    */
    }
}
